#include "Image.hpp"
#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) { };

TEST(GroupImage, Image1) 
{
    Image img(40,20);
    CHECK(img.getLargeur() == 40);
    CHECK(img.getHauteur() == 20);
}

TEST(GroupImage, Image2) 
{
    Image img(40,20);
	img.setPixel(30,10);
    CHECK(img.getPixel(0,30) == 10);
}



#ifndef IMAGE_
#define IMAGE_

#include <vector>
#include <string>
#include <iostream>

using namespace std;

class Image {
	private:
		int _largeur;
		int _hauteur;
		int* _pixels;
	public:
		Image(int largeur,int hauteur);
		Image(Image& img);
		~Image();
		const int& getLargeur()const;
		const int& getHauteur()const;
		
		int getPixel(int i,int j)const;
		void setPixel(int i,int couleur)const;
		
		 int& getPixel(int i,int j);
		
		
	
	};
	void ecrirePnm(const Image & img, const std::string & nomFichier);
	void remplir(Image & img);
	Image bordure( Image & img, int couleur);
	
	
#endif

